import { createEvent, createStore, Event, guard } from 'effector'
import { io, Socket } from 'socket.io-client'
import { createEffect, forward } from 'effector/effector.umd'
import { usePayload } from '../../lib/use-payload'
import { IRoomDto } from '../room-repository'

export interface IEventRepository {
  connect: Event<void>
  connected: Event<void>
  onlineUsersCountChanged: Event<number>
  currentUserRoomChanged: Event<IRoomDto | null>
  userJoinedToRoom: Event<void>
  userLeftFromRoom: Event<void>
  disconnect: Event<void>
}

export class EventRepository implements IEventRepository {
  public readonly connect = createEvent<void>()

  public readonly disconnect = createEvent<void>()

  public readonly connected = createEvent<void>()

  public readonly onlineUsersCountChanged = createEvent<number>()

  public readonly currentUserRoomChanged = createEvent<IRoomDto | null>()

  public readonly userJoinedToRoom = createEvent<void>()

  public readonly userLeftFromRoom = createEvent<void>()

  private readonly $socket = createStore<Socket | null>(null)

  private readonly connectSocketFx = createEffect<void, Socket>({
    handler: () => {
      const socket = io({ transports: ['websocket'] })

      socket.on('connect', this.connected)

      socket.on('current-room', this.currentUserRoomChanged)

      socket.on('online-count', this.onlineUsersCountChanged)

      socket.on('user-joined', this.userJoinedToRoom)

      socket.on('user-left', this.userLeftFromRoom)

      return socket
    },
  })

  private readonly disconnectSocketFx = createEffect({
    handler: (socket: Socket) => socket.disconnect(),
  })

  constructor() {
    this.$socket.on(this.connectSocketFx.doneData, usePayload)

    forward({
      from: this.connect,
      to: this.connectSocketFx,
    })

    forward({ from: this.connectSocketFx.done, to: this.connected })

    guard({
      source: this.$socket,
      clock: this.disconnect,
      filter: (socket): socket is Socket => socket !== null,
      target: this.disconnectSocketFx,
    })
  }
}
