export interface RepositoryError {
  statusCode: number
  message: string
  error: string
}
