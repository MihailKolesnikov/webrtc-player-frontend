import { Effect } from 'effector'
import { RepositoryError } from '../types'
import { IUserDto } from '../user-repository'

export interface IRoomListItemDto {
  id: string
  name: string
  public: boolean
  currentMembers: number
  maxMembers: number
}

export interface IRoomDto {
  id: string
  name: string
  public: boolean
  currentMembers: number
  maxMembers: number
  members: IUserDto[]
}

export interface IRoomsFilter {
  name?: string
}

export interface ICreateRoomDto {
  name: string
  public: boolean
  password: string
  maxMembers: number
}

export interface IUpdateRoomDto {
  name: string
  public: boolean
  password: string
  maxMembers: number
}

export interface IJoinRoomDto {
  roomId: string
  password: string
}

export interface IRoomRepository {
  getRoomsListFx: Effect<IRoomsFilter, IRoomListItemDto[], RepositoryError>
  getRoomsByIdFx: Effect<{ roomId: string }, IRoomDto, RepositoryError>
  createRoomFx: Effect<ICreateRoomDto, IRoomDto, RepositoryError>
  updateRoomFx: Effect<IUpdateRoomDto, IRoomDto, RepositoryError>
  removeRoomFx: Effect<{ roomId: string }, void, RepositoryError>
  joinRoomFx: Effect<IJoinRoomDto, IRoomDto, RepositoryError>
  leaveRoomFx: Effect<{ roomId: string }, void, RepositoryError>
}
