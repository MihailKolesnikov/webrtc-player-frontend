import { Effect, createEffect } from 'effector'
import { http } from '../../lib/http'
import { RepositoryError } from '../types'

export interface IUserDto {
  displayName: string
  avatarUrl: string
}

export interface ISignupRequestDto {
  username: string
  password: string
  displayName: string
}

export interface ILoginRequestDto {
  username: string
  password: string
}

export interface IUserRepository {
  signupFx: Effect<ISignupRequestDto, IUserDto, RepositoryError>
  loginFx: Effect<ILoginRequestDto, IUserDto, RepositoryError>
  logoutFx: Effect<void, void, RepositoryError>
  getUserFx: Effect<void, IUserDto, RepositoryError>
  updateUserFx: Effect<Partial<IUserDto>, IUserDto, RepositoryError>
}

export class UserRepository implements IUserRepository {
  public readonly loginFx = createEffect<ILoginRequestDto, IUserDto, RepositoryError>({
    handler: (loginDto) => http.post('/login', loginDto).then((res) => res.data),
  })

  public readonly logoutFx = createEffect<void, void, RepositoryError>({
    handler: () => http.get('/logout').then((res) => res.data),
  })

  public readonly signupFx = createEffect<ISignupRequestDto, IUserDto, RepositoryError>({
    handler: (signupDto) => http.post('/signup', signupDto).then((res) => res.data),
  })

  public readonly getUserFx = createEffect<void, IUserDto, RepositoryError>({
    handler: () => http.get('/profile').then((res) => res.data),
  })

  public readonly updateUserFx = createEffect<Partial<IUserDto>, IUserDto, RepositoryError>({
    handler: (userDto) => http.patch('/profile', userDto).then((res) => res.data),
  })
}
