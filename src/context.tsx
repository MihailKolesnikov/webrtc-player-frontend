import React from 'react'
import { DIContainer } from '@wessberg/di'
import { IAppService, AppService } from './services/app-service'
import { IUserService, UserService } from './services/user-service'
import { UserRepository, IUserRepository } from './repositories/user-repository'
import { IRouterService, RouterService } from './services/router-service'
import { EventRepository, IEventRepository } from './repositories/event-repository'

export const context = new DIContainer()

context.registerSingleton<IUserRepository, UserRepository>()
context.registerSingleton<IEventRepository, EventRepository>()
context.registerSingleton<IUserService, UserService>()
context.registerSingleton<IRouterService, RouterService>()
context.registerSingleton<IAppService, AppService>()

export const AppContext = React.createContext({
  get appService(): IAppService {
    return context.get<IAppService>()
  },
  get userService(): IUserService {
    return context.get<IUserService>()
  },
  get routerService(): IRouterService {
    return context.get<IRouterService>()
  },
})
