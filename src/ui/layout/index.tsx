import React from 'react'

interface LayoutProps extends React.HTMLAttributes<HTMLDivElement> {
  username: string
  onlineUsersCount: number
}

export const Layout = (props: LayoutProps) => {
  return (
    <div>
      <header>
        <p>Hi, {props.username}!</p>
        <p>Online users now: {props.onlineUsersCount}</p>
      </header>
      <div>{props.children}</div>
    </div>
  )
}
