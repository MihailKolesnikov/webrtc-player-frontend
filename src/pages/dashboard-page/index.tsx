import React from 'react'
import { AppContext } from '../../context'
import { useStore } from 'effector-react'

export const DashboardPage = () => {
  const { userService } = React.useContext(AppContext)

  const currentRoom = useStore(userService.$currentRoom)

  const userJoinedToRoom = currentRoom !== null

  return (
    <div>
      <p>Hi, i am dashboard cumponent</p>
      {userJoinedToRoom && <p>Your room is {currentRoom.name}</p>}
      <button onClick={() => userService.logout()}>logout</button>
    </div>
  )
}
