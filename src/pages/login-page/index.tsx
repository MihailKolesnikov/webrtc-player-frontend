import React from 'react'
import { AppContext } from '../../context'

interface LoginForm {
  username: string
  password: string
}

export const LoginPage = () => {
  const { userService } = React.useContext(AppContext)
  const [formValues, setFormValues] = React.useState<LoginForm>({ username: '', password: '' })

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    userService.logIn(formValues)
  }

  const onValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormValues((state) => ({ ...state, [e.target.name]: e.target.value }))
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input name='username' onChange={onValueChange} />
        <input name='password' type='password' onChange={onValueChange} />
        <button>Login</button>
      </form>
    </div>
  )
}
