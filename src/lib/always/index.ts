export function always<T>(value: T) {
  return () => value
}
