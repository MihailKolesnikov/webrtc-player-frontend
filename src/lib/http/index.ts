import axios, { AxiosError } from "axios"

export const http = axios.create({ baseURL: "/api" })

http.interceptors.response.use(undefined, (error: AxiosError) => {
  return Promise.reject(error.response?.data)
})
