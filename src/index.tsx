import React from "react"
import ReactDOM from "react-dom"
import { App } from "./App"
import { context } from "./context"
import { IAppService } from "./services/app-service"
import "./index.css"

const appService = context.get<IAppService>()

appService.run.watch(() => {
  ReactDOM.render(<App />, document.getElementById("root"))
})

appService.run()
