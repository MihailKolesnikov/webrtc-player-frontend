import history from 'history/browser'
import { createRouter } from 'trace-router'
import { Route, Router } from 'trace-router/dist/types'

export interface IRouter extends Router {}

export interface IRoutes {
  loginPage: Route
  dashboardPage: Route
}

export interface IRouterService {
  routes: IRoutes
  router: IRouter
}

export class RouterService implements IRouterService {
  public readonly router = createRouter({ history })

  public readonly routes = {
    loginPage: this.router.add('/login'),
    dashboardPage: this.router.add('/profile'),
  }
}
