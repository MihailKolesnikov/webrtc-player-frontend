import { createEvent, createStore, Event, forward, Store } from 'effector'
import { IUserRepository, ILoginRequestDto, IUserDto } from '../../repositories/user-repository'
import { usePayload } from '../../lib/use-payload'
import { always } from '../../lib/always'
import { IRouterService } from '../router-service'
import { useVoid } from '../../lib/use-void'
import { IEventRepository } from '../../repositories/event-repository'
import { IRoomDto } from '../../repositories/room-repository'

export interface IUserService {
  logIn: Event<ILoginRequestDto>
  loggedIn: Event<void>
  logInFailed: Event<void>
  logout: Event<void>
  getUserProfile: Event<void>
  $isLoggedIn: Store<boolean>
  $userProfile: Store<IUserDto | null>
  $userChecked: Store<boolean>
  $onlineUsersCount: Store<number>
  $currentRoom: Store<IRoomDto | null>
}

export class UserService implements IUserService {
  public readonly logIn = createEvent<ILoginRequestDto>()

  public readonly loggedIn = createEvent<void>()

  public readonly logInFailed = createEvent<void>()

  public readonly logout = createEvent<void>()

  public readonly getUserProfile = createEvent<void>()

  public readonly $userProfile = createStore<IUserDto | null>(null)

  public readonly $isLoggedIn = createStore<boolean>(false)

  public readonly $userChecked = createStore<boolean>(false)

  public readonly $onlineUsersCount = createStore<number>(0)

  public readonly $currentRoom = createStore<IRoomDto | null>(null)

  constructor(
    private readonly userRepository: IUserRepository,
    private readonly routerService: IRouterService,
    private readonly eventRepository: IEventRepository
  ) {
    this.$userChecked.on([this.userRepository.getUserFx.done, this.userRepository.getUserFx.fail], always(true))

    this.$userProfile
      .on([this.userRepository.getUserFx.doneData, this.userRepository.loginFx.doneData], usePayload)
      .on(this.logout, always(null))

    this.$isLoggedIn
      .on([this.userRepository.getUserFx.doneData, this.userRepository.loginFx.doneData], always(true))
      .on(this.logout, always(false))

    this.$onlineUsersCount.on(this.eventRepository.onlineUsersCountChanged, usePayload)

    this.$currentRoom.on(this.eventRepository.currentUserRoomChanged, usePayload)

    forward({ from: this.logIn, to: this.userRepository.loginFx })

    forward({ from: this.userRepository.loginFx.doneData, to: this.loggedIn.prepend(useVoid) })

    forward({ from: this.logout, to: [this.userRepository.logoutFx, this.eventRepository.disconnect] })

    forward({ from: this.getUserProfile, to: this.userRepository.getUserFx })

    forward({ from: this.userRepository.getUserFx.doneData, to: this.loggedIn })

    forward({
      from: [this.userRepository.getUserFx.failData, this.userRepository.loginFx.failData],
      to: this.logInFailed,
    })

    forward({
      from: this.loggedIn,
      to: [this.routerService.routes.dashboardPage.navigate, this.eventRepository.connect],
    })

    forward({
      from: [this.userRepository.getUserFx.fail, this.userRepository.logoutFx.done],
      to: this.routerService.routes.loginPage.navigate.prepend(useVoid),
    })

    this.eventRepository.connected.watch(() => console.log('socket connected'))
    this.eventRepository.disconnect.watch(() => console.log('socket disconnected'))
  }
}
