import { createEvent, forward, guard, Event, sample, Store, createStore } from 'effector'
import { IUserService } from '../user-service'
import { IRouterService } from '../router-service'
import { useVoid } from '../../lib/use-void'
import { not } from '../../lib/not'

export interface IAppService {
  run: Event<void>
}

export class AppService implements IAppService {
  public readonly run = createEvent<void>()

  constructor(private readonly userService: IUserService) {
    forward({
      from: this.run,
      to: this.userService.getUserProfile,
    })
  }
}
