import React, { useContext } from 'react'
import { AppContext } from '@context'
import { useStore } from 'effector-react'
import { Route } from 'trace-router-react'
import { LoginPage } from '@pages/login-page'
import { DashboardPage } from '@pages/dashboard-page'
import { Layout } from '@ui/layout'

export const App = () => {
  const { userService, routerService } = useContext(AppContext)

  const userChecked = useStore(userService.$userChecked)
  const userProfile = useStore(userService.$userProfile)
  const onlineUsersCount = useStore(userService.$onlineUsersCount)

  const hasNoMatchedRoute = useStore(routerService.router.noMatches)

  if (!userChecked) {
    return <div>Loading...</div>
  }

  return (
    <>
      <Route of={routerService.routes.loginPage}>
        <LoginPage />
      </Route>

      <Route of={routerService.routes.dashboardPage}>
        <Layout username={userProfile?.displayName ?? ''} onlineUsersCount={onlineUsersCount}>
          <DashboardPage />
        </Layout>
      </Route>

      {hasNoMatchedRoute && <div>Page not found</div>}
    </>
  )
}
